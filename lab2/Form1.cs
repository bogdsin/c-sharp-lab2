﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
    public partial class Form1 : Form
    {
        private bool calculation = false;
        private bool first_step = true;
        private double first_argument = 0;
        private int current_operation = 0;


        public Form1()
        {
            InitializeComponent();
            this.ActiveControl = textBox1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(calculatorKeyPress);
            this.KeyDown += new KeyEventHandler(calculatorKeyDown);
        }

        private void checkCalculation() {

            if (calculation == true)
            {
                calculation = false;
                textBox1.Text = "";
            }
        }

        private double performCalculation(double second_argument) {

            switch (current_operation)
            {
                case 1:
                    return first_argument + second_argument;
                case 2:
                    return first_argument - second_argument;
                case 3:
                    return first_argument * second_argument;
                case 4:
                    return first_argument / second_argument;
                case 5:
                    return first_argument % second_argument;
                case 6:
                    return Math.Pow(first_argument, second_argument);
                default:
                    return 0;           
            }
        }

        void calculatorKeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)48:
                case (char)96:
                    button0_Click(sender, e);
                    break;
                case (char)49:
                case (char)97:
                    button1_Click(sender, e);
                    break;
                case (char)50:
                case (char)98:
                    button2_Click(sender, e);
                    break;
                case (char)51:
                case (char)99:
                    button3_Click(sender, e);
                    break;
                case (char)52:
                case (char)100:
                    button4_Click(sender, e);
                    break;
                case (char)53:
                case (char)101:
                    button5_Click(sender, e);
                    break;
                case (char)54:
                case (char)102:
                    button6_Click(sender, e);
                    break;
                case (char)55:
                case (char)103:
                    button7_Click(sender, e);
                    break;
                case (char)56:
                    if (Control.ModifierKeys != Keys.Shift)
                        button8_Click(sender, e);
                    break;
                case (char)104:
                    button8_Click(sender, e);
                    break;
                case (char)57:
                case (char)105:
                    button9_Click(sender, e);
                    break;
            }            
        }

        void calculatorKeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Oemplus)
            {
                buttonPlus_Click(sender, e);
            }
            if (e.KeyCode == Keys.Divide || e.KeyCode == Keys.OemQuestion)
            {
                buttonDiv_Click(sender, e);
            }
            if (e.KeyCode == Keys.Multiply || (e.KeyCode == Keys.D8 && Control.ModifierKeys == Keys.Shift))
            {
                buttonMult_Click(sender, e);
            }
            if (e.KeyCode == Keys.OemMinus || e.KeyCode == Keys.Subtract)
            {
                buttonMinus_Click(sender, e);
            }

            if (e.KeyCode == Keys.Enter)
            {
                buttonEqu_Click(sender, e);
            }

            if (e.KeyCode == Keys.Separator || e.KeyCode == Keys.Oemcomma)
            {
                buttonDot_Click(sender, e);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 1;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 5;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 2;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 3;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 4;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 6;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 7;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 8;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 9;
        }

        private void button0_Click(object sender, EventArgs e)
        {
            checkCalculation();
            textBox1.Text += 0;
        }

        private void buttonDot_Click(object sender, EventArgs e)
        {
            checkCalculation();
            if (textBox1.Text.Length > 0 && (textBox1.Text.IndexOf(".") < 0))
                textBox1.Text += ',';
        }

        private void buttonPi_Click(object sender, EventArgs e)
        {
            checkCalculation();
            if (textBox1.Text.Length == 0)
                 textBox1.Text += 3.14;
        }

        private void buttonSqrt_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = Math.Sqrt(input);
                textBox1.Text = result.ToString();
            }
             
        }

        private void buttonSin_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = Math.Sin(input);
                textBox1.Text = result.ToString();
            }
        }

        private void buttonCos_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = Math.Cos(input);
                textBox1.Text = result.ToString();
            }
        }

        private void buttonTan_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = Math.Tan(input);
                textBox1.Text = result.ToString();
            }
        }

        private void buttonDivOne_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = 1 / input;
                textBox1.Text = result.ToString();
            }
        }

        private void buttonSquare_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text.ToString());
                double result = Math.Pow(input, 2);
                textBox1.Text = result.ToString();
            }
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            first_step = true;
            first_argument = 0;
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void buttonSign_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = input * -1;
                textBox1.Text = result.ToString();
            }
        }

        private void buttonAbs_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = Math.Abs(input);
                textBox1.Text = result.ToString();
            }
        }

        private void buttonLn_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double input = double.Parse(textBox1.Text);
                double result = Math.Log(input);
                textBox1.Text = result.ToString();
            }

        }

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                if (first_step)
                {
                    first_argument = double.Parse(textBox1.Text);
                    first_step = false;
                    current_operation = 1;
                }
                else
                {
                    double result = performCalculation(double.Parse(textBox1.Text));
                    textBox1.Text = result.ToString();
                    current_operation = 1;
                    first_argument = result;
                }
                
            }
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                if (first_step)
                {
                    first_argument = double.Parse(textBox1.Text);
                    first_step = false;
                    current_operation = 2;
                }
                else
                {
                    double result = performCalculation(double.Parse(textBox1.Text));
                    textBox1.Text = result.ToString();
                    current_operation = 2;
                    first_argument = result;
                }

            }

        }

        private void buttonMult_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                if (first_step)
                {
                    first_argument = double.Parse(textBox1.Text);
                    first_step = false;
                    current_operation = 3;
                }
                else
                {
                    double result = performCalculation(double.Parse(textBox1.Text));
                    textBox1.Text = result.ToString();
                    current_operation = 3;
                    first_argument = result;
                }

            }

        }

        private void buttonDiv_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                if (first_step)
                {
                    first_argument = double.Parse(textBox1.Text);
                    first_step = false;
                    current_operation = 4;
                }
                else
                {
                    double result = performCalculation(double.Parse(textBox1.Text));
                    textBox1.Text = result.ToString();
                    current_operation = 4;
                    first_argument = result;
                }

            }
        }

        private void buttonPercent_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                if (first_step)
                {
                    first_argument = double.Parse(textBox1.Text);
                    first_step = false;
                    current_operation = 5;
                }
                else
                {
                    double result = performCalculation(double.Parse(textBox1.Text));
                    textBox1.Text = result.ToString();
                    current_operation = 5;
                    first_argument = result;
                }

            }
        }

        private void buttonPow_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                if (first_step)
                {
                    first_argument = double.Parse(textBox1.Text);
                    first_step = false;
                    current_operation = 6;
                }
                else
                {
                    double result = performCalculation(double.Parse(textBox1.Text));
                    textBox1.Text = result.ToString();
                    current_operation = 6;
                    first_argument = result;
                }

            }
        }

        private void buttonEqu_Click(object sender, EventArgs e)
        {
            calculation = true;
            if (textBox1.Text.Length > 0)
            {
                double result = performCalculation(double.Parse(textBox1.Text));
                textBox1.Text = result.ToString();
                first_step = true;
                first_argument = 0;
            }
          }

        }
    }
